﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NYTArticlesNews.Entitys
{
    public class NewsArticle
    {
        public DateTime PublishDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public NewsArticle (DateTime publishDate, string title, string description)
        {
            PublishDate = publishDate;
            Title = title;
            Description = description;
        }
    }
}
