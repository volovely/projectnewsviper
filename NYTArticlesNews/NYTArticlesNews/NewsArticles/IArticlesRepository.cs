﻿using NYTArticlesNews.Entitys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NYTArticlesNews.NewsArticles
{
    public interface IArticlesRepository
    {
        Task<NewsArticle> GetNextArticle();
        Task<NewsArticle> GetPrevArticle();
    }
}
