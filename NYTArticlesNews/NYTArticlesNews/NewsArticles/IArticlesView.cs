﻿using NYTArticlesNews.Entitys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NYTArticlesNews.NewsArticles
{
    public interface IArticlesView
    {
        void SetRouter(IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> router);
        void UpdateArticle(NewsArticle article);
        void ShowSpinner();
        void HideSpinner();
    }
}
