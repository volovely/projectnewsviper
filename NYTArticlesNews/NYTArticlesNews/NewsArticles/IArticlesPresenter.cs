﻿using NYTArticlesNews.Entitys;
using NYTArticlesNews.Interfaces;
using System.Threading.Tasks;

namespace NYTArticlesNews.NewsArticles
{
    public interface IArticlesPresenter : ILifecycleView
    {
        void ButtonNextPressed();
        void ButtonPrevPressed();
        void UpdateArticle(NewsArticle article);
        void ShowSpinner();
        void HideSpinner();
    }
}
