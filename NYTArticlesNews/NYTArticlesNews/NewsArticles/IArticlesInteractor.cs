﻿using NYTArticlesNews.Entitys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NYTArticlesNews.NewsArticles
{
    public interface IArticlesInteractor
    {
        Task<NewsArticle> GetNextArticle();
        Task<NewsArticle> GetPrevArticle();
    }
}
