﻿using System;
using System.Collections.Generic;
using System.Text;
using NYTArticlesNews.Entitys;
using NYTArticlesNews.NewsArticles;
using System.Threading.Tasks;

namespace NYTArticlesNews.NewsArticlesImplementation
{
    public class ArticlesPresenter : IArticlesPresenter
    {
        private IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> _articlesRouter;

        public ArticlesPresenter(IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> articlesRouter)
        {
            _articlesRouter = articlesRouter;
        }
        public async void ButtonNextPressed()
        {
            ShowSpinner();
            var article = await _articlesRouter.Interactor?.GetNextArticle();
            UpdateArticle(article);
        }

        public async void ButtonPrevPressed()
        {
            ShowSpinner();
            var article = await _articlesRouter.Interactor?.GetPrevArticle();
            UpdateArticle(article);
        }

        public void UpdateArticle(NewsArticle article)
        {
            HideSpinner();
            _articlesRouter.View?.UpdateArticle(article);
        }

        public void ViewWillAppear()
        {
            return;
        }

        public void ViewWillDisappear()
        {
            return;
        }

        public void ViewWillRotate()
        {
            return;
        }

        public void ShowSpinner()
        {
            _articlesRouter.View?.ShowSpinner();
        }

        public void HideSpinner()
        {
            _articlesRouter.View?.HideSpinner();
        }
    }
}
