﻿using NYTArticlesNews.NewsArticles;

namespace NYTArticlesNews.NewsArticlesImplementation
{
    public class ArticlesRouter : IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView>
    {
        private IArticlesInteractor _interactor;
        private IArticlesPresenter _presenter;
        private IArticlesView _view;

        public ArticlesRouter()
        {

        }

        public ArticlesRouter (IArticlesInteractor interactor, IArticlesPresenter presenter, IArticlesView view)
        {
            _interactor = interactor;
            _presenter = presenter;
            _view = view;
        }

        public IArticlesInteractor Interactor
        {
            get
            {
                return _interactor;
            }
            set
            {
                _interactor = value;
            }
        }

        public IArticlesPresenter Presenter
        {
            get
            {
                return _presenter;
            }

            set
            {
                _presenter = value;
            }
        }

        public IArticlesView View
        {
            get
            {
                return _view;
            }

            set
            {
                _view = value;
            }
        }
    }
}
