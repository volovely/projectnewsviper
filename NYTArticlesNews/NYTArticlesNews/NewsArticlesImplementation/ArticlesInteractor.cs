﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NYTArticlesNews.Entitys;
using NYTArticlesNews.NewsArticles;

namespace NYTArticlesNews.NewsArticlesImplementation
{
    public class ArticlesInteractor : IArticlesInteractor
    {
        IArticlesRepository _repos;

        public ArticlesInteractor(IArticlesRepository repos)
        {
            _repos = repos;
        }
        public async Task<NewsArticle> GetNextArticle()
        {
           return await _repos?.GetNextArticle();
        }

        public async Task<NewsArticle> GetPrevArticle()
        {
            return await _repos?.GetPrevArticle();
        }
    }
}
