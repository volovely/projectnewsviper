﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NYTArticlesNews.Entitys;
using NYTArticlesNews.NewsArticles;

namespace NYTArticlesNews.NewsArticlesImplementation
{
    public class ArticlesRepository : IArticlesRepository
    {
        private int _currentArticle = -1;
        private object _newsLocker = new object();
        private List<NewsArticle> _news = new List<NewsArticle>();
        private List<NewsArticle> News
        {
            get
            {
                lock (_newsLocker)
                {
                    return _news;
                }
            }

            set
            {
                lock (_newsLocker)
                {
                    _news = value;
                }
            }
        }
        public async Task<NewsArticle> GetNextArticle()
        {
            _currentArticle++;
            if (_currentArticle < News.Count)
            {
                return News[_currentArticle];
            }
            else
            {
                await GetMoreNews();
                return News[_currentArticle];
            }

        }

        public async Task<NewsArticle> GetPrevArticle()
        {
            _currentArticle--;
            if (_currentArticle < 0)
            {
                if (News.Count == 0)
                {
                    await GetMoreNews();
                }
                _currentArticle = 0;
                return News[_currentArticle];
            }

            return News[_currentArticle];
        }

        //Mock Method. 
        private async Task GetMoreNews()
        {
            await Task.Run(() =>
            {
                for (int i = _currentArticle; i < _currentArticle + 5; i++)
                {
                    News.Add(new NewsArticle(DateTime.Now, "Title " + i, "Description " + i));
                    Task.Delay(1000).Wait();
                }
            });
        }
    }
}
