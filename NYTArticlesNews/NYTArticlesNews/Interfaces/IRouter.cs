﻿namespace NYTArticlesNews
{
    public interface IRouter<TInteractor, TPresenter, TView>
    {
        TInteractor Interactor { get; set; }
        TPresenter Presenter { get; set; }
        TView View { get; set; }
    }
}
