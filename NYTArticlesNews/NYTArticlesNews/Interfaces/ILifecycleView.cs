﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NYTArticlesNews.Interfaces
{
    public interface ILifecycleView
    {
        void ViewWillAppear();
        void ViewWillDisappear();
        void ViewWillRotate();
    }
}
