﻿using Foundation;
using System;
using System.ComponentModel;
using UIKit;
using NYTArticlesNews.NewsArticles;
using NYTArticlesNews.Entitys;

namespace NYTArticlesNews.iOS
{
    [DesignTimeVisible(true)]
    public partial class ArticlesView : UIView, IComponent, IArticlesView
    {
        public ArticlesView (IntPtr handle) : base (handle)
        {
        }

		IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> _router;

        #region IComponent implementation

        public ISite Site { get; set; }
        public event EventHandler Disposed;

        #endregion

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            if ((Site != null) && Site.DesignMode)
            {
                // Bundle resources aren't available in DesignMode
                return;
            }

            NSBundle.MainBundle.LoadNib("ArticlesView", this, null);

            // At this point all of the code-behind properties should be set, specifically rootView which must be added as a subview of this view
            var frame = _rootView.Frame;
            frame.Height = Frame.Height;
            frame.Width = Frame.Width;
            _rootView.Frame = frame;
            this.AddSubview(this._rootView);
            SetButtonsActions();
        }

        public void HideSpinner()
        {
            _pb.Hidden = true;
            SetButtonsState(true);
        }

        public void ShowSpinner()
        {
            _pb.Hidden = false;
            _pb.StartAnimating();
            SetButtonsState(false);
        }

        public void UpdateArticle(NewsArticle article)
        {
            _lblTitle.Text = article.Title;
            _tvDescriprion.Text = article.Description;
        }

        private void SetButtonsState(bool state)
        {
            _btnNext.Enabled = state;
            _btnPrev.Enabled = state;
        }

        private void SetButtonsActions()
        {
            _btnNext.TouchUpInside +=  (s, e) =>
            {
                _router.Presenter?.ButtonNextPressed();
            };

            _btnPrev.TouchUpInside += (s, e) =>
            {
                _router.Presenter?.ButtonPrevPressed();
            };
        }

		public void SetRouter(IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> router)
		{
			_router = router;
		}
	}
}