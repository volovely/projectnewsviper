﻿using NYTArticlesNews.NewsArticles;
using NYTArticlesNews.NewsArticlesImplementation;
using System;

using UIKit;
using NYTArticlesNews;

namespace NYTArticlesNews.iOS
{
	public partial class ViewController : UIViewController
	{
        private IArticlesPresenter _articlesPresenter;
        private IArticlesInteractor _articlesInteractor;
		private IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> _router;

        public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			_router = new ArticlesRouter();
            _articlesInteractor = new ArticlesInteractor(DAL.ArticlesRepository);
			_articlesPresenter = new ArticlesPresenter(_router);
			_articlesView.SetRouter(_router);
			_router.Interactor = _articlesInteractor;
			_router.Presenter = _articlesPresenter;
			_router.View = _articlesView;
        }

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            _articlesPresenter.ViewWillAppear();
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            _articlesPresenter.ViewWillDisappear();
        }

        public override void WillRotate(UIInterfaceOrientation toInterfaceOrientation, double duration)
        {
            base.WillRotate(toInterfaceOrientation, duration);
            _articlesPresenter.ViewWillRotate();
        }
    }
}

