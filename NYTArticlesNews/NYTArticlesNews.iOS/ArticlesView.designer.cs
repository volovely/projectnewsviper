﻿// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace NYTArticlesNews.iOS
{
    [Register ("ArticlesView")]
    partial class ArticlesView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnNext { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton _btnPrev { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _lblTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView _pb { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView _rootView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView _tvDescriprion { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_btnNext != null) {
                _btnNext.Dispose ();
                _btnNext = null;
            }

            if (_btnPrev != null) {
                _btnPrev.Dispose ();
                _btnPrev = null;
            }

            if (_lblTitle != null) {
                _lblTitle.Dispose ();
                _lblTitle = null;
            }

            if (_pb != null) {
                _pb.Dispose ();
                _pb = null;
            }

            if (_rootView != null) {
                _rootView.Dispose ();
                _rootView = null;
            }

            if (_tvDescriprion != null) {
                _tvDescriprion.Dispose ();
                _tvDescriprion = null;
            }
        }
    }
}