using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using NYTArticlesNews.NewsArticles;
using NYTArticlesNews.Entitys;
using System.Threading.Tasks;

namespace NYTArticlesNews.Droid.NewsArticlesImplementation
{
    [Register("NYTArticlesNews.Droid.NewsArticlesImplementation.ArticlesView")]
    public class ArticlesView : RelativeLayout, IArticlesView
    {
        private Button _btnNext;
        private Button _btnPrev;
        private TextView _tvTitle;
        private TextView _tvDescription;
        private ProgressBar _pb;

        private IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> _router;
        public ArticlesView(Context context) : base(context)
        {
            Init(context);
        }

        public ArticlesView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init(context);
        }

        public ArticlesView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            Init(context);
        }

        public ArticlesView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
            Init(context);
        }

        public void HideSpinner()
        {
            _pb.Visibility = ViewStates.Gone;
            ButtonsState(true);
        }

        public void ShowSpinner()
        {
            _pb.Visibility = ViewStates.Visible;
            ButtonsState(false);
        }

        public void UpdateArticle(NewsArticle article)
        {
            //((Activity)Application.Context).RunOnUiThread(() =>
            //{
                _tvTitle.Text = article.Title;
                _tvDescription.Text = article.Description;
            //});
        }

        private void Init(Context context)
        {
            LayoutInflater inflater = ((Activity)context).LayoutInflater;
            inflater.Inflate(Resource.Layout.NewsArticle, this, true);
            FindViews();
            SetButtonsActions();
        }

        private void FindViews()
        {
            _btnNext = FindViewById<Button>(Resource.Id.btnNext);
            _btnPrev = FindViewById<Button>(Resource.Id.btnPrev);
            _tvTitle = FindViewById<TextView>(Resource.Id.tvTitle);
            _tvDescription = FindViewById<TextView>(Resource.Id.tvDescription);
            _pb = FindViewById<ProgressBar>(Resource.Id.progressBar);
        }

        private void ButtonsState(bool state)
        {
            _btnNext.Enabled = state;
            _btnPrev.Enabled = state;
        }

        private void SetButtonsActions()
        {
            _btnNext.Click += (s, e) =>
            {
                _router.Presenter?.ButtonNextPressed();
            };

            _btnPrev.Click += (s, e) =>
            {
                _router.Presenter?.ButtonPrevPressed();
            };
        }

        public void SetRouter(IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> router)
        {
            _router = router;
        }
    }
}