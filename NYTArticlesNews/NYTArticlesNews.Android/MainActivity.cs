﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using NYTArticlesNews.NewsArticles;
using NYTArticlesNews.Droid.NewsArticlesImplementation;
using NYTArticlesNews.NewsArticlesImplementation;

namespace NYTArticlesNews.Droid
{
	[Activity (Label = "NYTArticlesNews.Android", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
        private IArticlesView _articlesView;
        private IArticlesPresenter _articlesPresenter;
        private IArticlesInteractor _articlesInteractor;
        private IRouter<IArticlesInteractor, IArticlesPresenter, IArticlesView> _articlesRouter;

        protected override void OnCreate (Bundle bundle)
		{

			base.OnCreate (bundle);

			SetContentView (Resource.Layout.Main);

            _articlesRouter = new ArticlesRouter();
            _articlesView = FindViewById<ArticlesView>(Resource.Id.articlesView);
  
            _articlesInteractor = new ArticlesInteractor(DAL.ArticlesRepository);
            _articlesView.SetRouter(_articlesRouter);
            _articlesPresenter = new ArticlesPresenter(_articlesRouter);

            _articlesRouter.Interactor = _articlesInteractor;
            _articlesRouter.Presenter = _articlesPresenter;
            _articlesRouter.View = _articlesView;
        }

        protected override void OnResume()
        {
            base.OnResume();
            _articlesPresenter.ViewWillAppear();
        }

        protected override void OnPause()
        {
            base.OnPause();
            _articlesPresenter.ViewWillDisappear();
        }
    }
}


